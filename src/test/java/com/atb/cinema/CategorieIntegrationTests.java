package com.atb.cinema;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.*;

import static org.assertj.core.api.Assertions.*;
import com.atb.cinema.dao.CategorieRepository;
import com.atb.cinema.service.CategorieService;
import com.atb.cinema.serviceImp.CategorieServiceImp;
import com.atb.cinema.entities.Categorie;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MockitoJUnitRunner.class)
@DataJpaTest
public class CategorieIntegrationTests {
	@Autowired
	@InjectMocks
    CategorieServiceImp service;
  
	@Mock
	@Autowired
    CategorieRepository categorieRepository;
	
	@Autowired
	private TestEntityManager entityManager;
	
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void should_find_with_name_ending_population_less_than() {
    	
    	List<Categorie> list = new ArrayList<Categorie>();
        Categorie c1 = new Categorie(1L, "steve", null);
        list.add(c1);
        when(categorieRepository.findAll()).thenReturn(list);
        
        
      //test
        List<Categorie> empList = service.findAll();
         
        assertEquals(1, empList.size());
        assertThat(empList).isNotNull();
    }
    @Test
    public void getAllCategoriesTest()
    {
        List<Categorie> list = new ArrayList<Categorie>();
        Categorie c1 = new Categorie(1L, "steve", null);
        Categorie c2 = new Categorie(2L, "alexk@yahoo.com", null);
        Categorie c3 = new Categorie(3L, "Steve", null);
         
        list.add(c1);
        list.add(c2);
        list.add(c3);
         
        when(categorieRepository.findAll()).thenReturn(list);
         
        //test
        List<Categorie> empList = service.findAll();
         
        assertEquals(3, empList.size());
        verify(categorieRepository, times(1)).findAll();
    } 
   

}
