package com.atb.cinema.acceptanceTests;

import org.springframework.test.util.ReflectionTestUtils;

import com.atb.cinema.entities.Categorie;

/**
 * @author Petri Kainulainen
 */
public class CategorieBuilder {

    private Categorie model;

    public CategorieBuilder() {
        model = new Categorie();
    }

    public CategorieBuilder id(Long id) {
        ReflectionTestUtils.setField(model, "id", id);
        return this;
    }

    public CategorieBuilder name(String name) {
    	ReflectionTestUtils.setField(model, "name", name);
        return this;
    }

    

    public Categorie build() {
        return model;
    }
}