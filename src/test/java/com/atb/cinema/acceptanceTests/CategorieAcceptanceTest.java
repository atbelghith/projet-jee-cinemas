package com.atb.cinema.acceptanceTests;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.atb.cinema.dao.CategorieRepository;
import com.atb.cinema.service.CategorieService;
import com.atb.cinema.serviceImp.CategorieServiceImp;
import com.atb.cinema.serviceImp.TestUtil;
import com.atb.cinema.entities.*;

import javassist.NotFoundException;

@RunWith(SpringJUnit4ClassRunner.class)
public class CategorieAcceptanceTest {

	
	  private MockMvc mockMvc;
	  
	/*
	 * @Mock private CategorieServiceImp service;
	 * 
	 * //@InjectMocks //private CategorieRepository categorieRepository;
	 * 
	 * @Before public void setUp() throws Exception{ mockMvc =
	 * MockMvcBuilders.standaloneSetup(service).build(); }
	 * 
	 * 
	 * @Test public void testCategorie() throws Exception {
	 * 
	 * Mockito.when(service.findAll()).thenReturn(null);
	 * 
	 * mockMvc.perform(get("/categories").accept(MediaType.APPLICATION_JSON))
	 * .andExpect(status().isOk()) .andExpect((ResultMatcher) jsonPath("$.*",
	 * Matchers.hasSize(3))); //.andExpect(jsonPath("$.title",
	 * Matchers.is("categories");
	 * 
	 * }
	 * 
	 * 
	 * @Test public void findById_TodoEntryNotFound_ShouldReturnHttpStatusCode404()
	 * throws Exception { when(service.findById(1L)).thenThrow(new
	 * NotFoundException(""));
	 * 
	 * mockMvc.perform(get("/categories/{id}", 1L))
	 * .andExpect(status().isNotFound());
	 * 
	 * verify(service).findById(1L); verifyNoMoreInteractions(service); }
	 * 
	 * @Test public void
	 * add_TitleAndDescriptionAreTooLong_ShouldReturnValidationErrorsForTitleAndDescription
	 * () throws Exception { String name = TestUtil.createStringWithLength(101);
	 * 
	 * 
	 * Categorie dto = new CategorieBuilder() .name(name) .build();
	 * 
	 * mockMvc.perform(post("/categories")
	 * .contentType(TestUtil.APPLICATION_JSON_UTF8)
	 * .content(TestUtil.convertObjectToJsonBytes(dto)) )
	 * .andExpect(status().isBadRequest())
	 * .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
	 * .andExpect(jsonPath("$.fieldErrors", hasSize(2))) //
	 * .andExpect(jsonPath("$.fieldErrors[*].path", containsInAnyOrder( "name")))
	 * .andExpect(jsonPath("$.fieldErrors[*].message", containsInAnyOrder(
	 * "The maximum length of the description is 500 characters.",
	 * "The maximum length of the title is 100 characters." )));
	 * 
	 * verifyZeroInteractions(service); }
	 */
	 
	@InjectMocks
	CategorieServiceImp manager;
     
    @Mock
    CategorieRepository dao;
 
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }
     
    @Test
    public void getAllEmployeesTest()
    {
        List<Categorie> list = new ArrayList<Categorie>();
        
        
        Categorie catOne = new Categorie(1L, "John", null);
        Categorie empTwo = new Categorie(2L, "Alex", null);
        Categorie empThree = new Categorie(3L, "Steve", null);
         
        list.add(catOne);
        list.add(empTwo);
        list.add(empThree);
         
        when(dao.findAll()).thenReturn(list);
         
        //test
        List<Categorie> empList = manager.findAll();
         
        assertEquals(3, ( empList).size());
        verify(dao, times(1)).findAll();
    }
     
    @Test
    public void getEmployeeByIdTest(){
    	
    	when(dao.findById(1L)).thenReturn( Optional.of(new Categorie(1L, "steve", null)));   
    	System.out.print(dao.findById(1L).get());
        Categorie cat = manager.findById(1L);
         
        assertEquals("steve", cat.getName());
        
       
    }
     
    @Test
    public void createEmployeeTest()
    {
        Categorie cat = new Categorie(1L,"Lokesh",null);
         
        manager.save(cat);
         
        verify(dao, times(1)).save(cat);
    }
    @Test
    public void deleteEmployeeTest()
    {
        Categorie cat = new Categorie(1L,"Lokesh",null);
         
        manager.delete(cat);
         
        verify(dao, times(1)).delete(cat);
    }


}
