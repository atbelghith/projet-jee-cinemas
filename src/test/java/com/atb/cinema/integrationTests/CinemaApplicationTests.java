package com.atb.cinema.integrationTests;


  
  import org.junit.jupiter.api.Test; 
  import org.springframework.boot.test.context.SpringBootTest;
  import org.springframework.jdbc.core.JdbcTemplate;
  import org.junit.runner.RunWith; 
  import org.springframework.beans.factory.annotation.Autowired; 
  import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest; 
  import org.springframework.test.context.junit4.SpringRunner;
  
  import com.atb.cinema.dao.CategorieRepository; 
  import com.atb.cinema.entities.Categorie; 
  import com.atb.cinema.service.CategorieService;
  import com.atb.cinema.serviceImp.CategorieServiceImp;

  import lombok.var;
  
  import static org.assertj.core.api.Assertions.assertThat;
  
  import java.util.List;

  import javax.persistence.EntityManager;
  import javax.sql.DataSource;
  
  @RunWith(SpringRunner.class)
  
  @DataJpaTest
  
  class CinemaApplicationTests {
  
  @Autowired 
  private CategorieRepository repository;
  

  @Autowired 
  private DataSource dataSource;
  @Autowired 
  private JdbcTemplate jdbcTemplate;
  @Autowired 
  private EntityManager entityManager;
  
  @Test 
  public void should_find_with_name_ending_population_less_than() {
  
  //List<Categorie> list = repository.findAll();
  
  assertThat(dataSource).isNotNull();
  assertThat(jdbcTemplate).isNotNull();
  assertThat(entityManager).isNotNull();
  assertThat(repository).isNotNull();
  
  }
  
  @Test 
  public void whenFindByName_thenReturnEmployee() { 
	  // given Categorie
     Categorie alex = new Categorie(null, "alex",null);
     
     entityManager.persist(alex);
     entityManager.flush();
  
     // when
      Categorie repo = repository.findByName(alex.getName());
  
     // then 
     assertThat(repo.getName()) .isEqualTo(alex.getName());
   }
  }
 