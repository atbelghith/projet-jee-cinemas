package com.atb.cinema.integrationTests;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.atb.cinema.dao.CategorieRepository;
import com.atb.cinema.service.CategorieService;
import com.atb.cinema.serviceImp.CategorieServiceImp;
import com.atb.cinema.entities.Categorie;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MockitoJUnitRunner.class)
public class CategorieIntegrationTest {
	@InjectMocks
    CategorieServiceImp service;
     
    @Mock
    CategorieRepository categorieRepository;
     
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }
 
    @Test
    public void getAllCategoriesTest()
    {
        List<Categorie> list = new ArrayList<Categorie>();
        Categorie c1 = new Categorie(1L, "steve", null);
        Categorie c2 = new Categorie(2L, "alexk@yahoo.com", null);
        Categorie c3 = new Categorie(3L, "Steve", null);
         
        list.add(c1);
        list.add(c2);
        list.add(c3);
         
        when(categorieRepository.findAll()).thenReturn(list);
         
        //test
        List<Categorie> empList = service.findAll();
         
        assertEquals(3, empList.size());
        verify(categorieRepository, times(1)).findAll();
    } 

}
