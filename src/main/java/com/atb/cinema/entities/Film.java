package com.atb.cinema.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor
@JsonIgnoreProperties
public class Film implements Serializable {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	private String title;
	private double duree;
	private String realisateur;
	private String description;
	private String photo;
	private Date dateSortie;
	@JsonProperty(access = Access.WRITE_ONLY)
	@OneToMany(mappedBy = "film")
	Collection<Projection> projections;
	@JsonProperty(access = Access.WRITE_ONLY)
	@ManyToOne
	private Categorie categorie;
}
