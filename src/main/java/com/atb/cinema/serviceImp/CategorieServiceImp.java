package com.atb.cinema.serviceImp;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.atb.cinema.dao.CategorieRepository;
import com.atb.cinema.entities.Categorie;
import com.atb.cinema.service.CategorieService;
import com.atb.cinema.service.PageService;

@Service
public class CategorieServiceImp implements  CategorieService{
    
	@Autowired
	private CategorieRepository categorieRepository;

	@Override
	public Categorie saveOrUpdate(Categorie c) {
		
		return categorieRepository.save(c);
	}

	@Override
	public void deleteById( Long id) {
		// TODO Auto-generated method stub
		categorieRepository.deleteById(id);
	}

	@Override
	public Categorie findById( Long id) {
		// TODO Auto-generated method stub
		return categorieRepository.findById(id).get();
	}

	

	@Override
	public void delete(Categorie c) {
		// TODO Auto-generated method stub
		categorieRepository.delete(c);
	}

	@Override
	public List<Categorie> findAll() {
		// TODO Auto-generated method stub
		return categorieRepository.findAll();
	}
	@Override
	public Page<Categorie> findAllPage(int page, int size) {
		// TODO Auto-generated method stub
		return categorieRepository.findAll(PageRequest.of(page,size));
	}

	@Override
	public PageService<Categorie> getCategories(int page, int size) {
		// TODO Auto-generated method stub
		    PageService<Categorie> pCat =new PageService<Categorie>();
			Page<Categorie> cats = categorieRepository.findAll(PageRequest.of(page,size));
			
			pCat.setContent(cats.getContent());
			pCat.setPage(cats.getNumber());
			pCat.setNumberOfElements(cats.getNumberOfElements());
			pCat.setTotalPages(cats.getTotalPages());
			pCat.setTotalElements((int) cats.getTotalElements());
			return pCat;
			
			
	}

	@Override
	public List<Categorie> getcategoriefilms(Long id) {
		// TODO Auto-generated method stub
		return categorieRepository.getcategoriefilms(id);
	}
	
	/**
	 * this method is use for updating categorie
	 * 
	 * @param id: the id of the updated user
	 * @param categorie: the new user object with the new values
	 * @return the updated categorie
	 */
	@Override
	public Categorie update(Long id, Categorie categorie) {
		//Categorie c =categorieRepository.findById(id).get();
		categorie.setId(id);
		return categorieRepository.saveAndFlush(categorie);
	}


	@Override
	public Categorie save(Categorie c) {
		// TODO Auto-generated method stub
		return categorieRepository.save(c);
	}
	

}
