package com.atb.cinema.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.atb.cinema.dao.CinemaRepository;
import com.atb.cinema.entities.Cinema;
import com.atb.cinema.entities.Film;
import com.atb.cinema.service.CinemaService;
import com.atb.cinema.service.PageService;
@Service
public class CinemaServiceIml implements CinemaService {
    @Autowired
     private CinemaRepository cinemaRepository ;

	@Override
	public Cinema saveOrUpdate(Cinema c) {
		// TODO Auto-generated method stub
		return cinemaRepository.save(c);
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		cinemaRepository.deleteById(id);
	}

	@Override
	public Cinema findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Cinema> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Cinema t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Page<Cinema> findAllPage(int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}

	

	@Override
	public Cinema update(Long id, Cinema t) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Cinema save(Cinema t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PageService<Film> getFilms(int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}

}
