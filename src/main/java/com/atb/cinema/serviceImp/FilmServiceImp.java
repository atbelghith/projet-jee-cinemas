package com.atb.cinema.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.atb.cinema.dao.FilmRepository;
import com.atb.cinema.entities.Film;
import com.atb.cinema.service.FilmService;

@Service
public class FilmServiceImp implements  FilmService{
    
	@Autowired
	private FilmRepository filmRepository;

	@Override
	public Film saveOrUpdate(Film f) {
		
		return filmRepository.save(f);
	}

	@Override
	public void deleteById( Long id) {
		// TODO Auto-generated method stub
		filmRepository.deleteById(id);
	}

	@Override
	public Film findById( Long id) {
		// TODO Auto-generated method stub
		return filmRepository.findById(id).get();
	}

	

	@Override
	public void delete(Film c) {
		// TODO Auto-generated method stub
		filmRepository.delete(c);
	}

	@Override
	public List<Film> findAll() {
		// TODO Auto-generated method stub
		return filmRepository.findAll();
	}
	@Override
	public Page<Film> findAllPage(int page, int size) {
		// TODO Auto-generated method stub
		return filmRepository.findAll(PageRequest.of(page,size));
	}

	@Override
	public List<Film> getfilms(Long id) {
		// TODO Auto-generated method stub
		    
		 return  filmRepository.getFilms(id);
			
			
			
			
	}

	@Override
	public Film update(Long id, Film t) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Film save(Film t) {
		// TODO Auto-generated method stub
		return null;
	}



	

}
