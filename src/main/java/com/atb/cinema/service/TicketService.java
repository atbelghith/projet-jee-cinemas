package com.atb.cinema.service;

import com.atb.cinema.entities.Ticket;

public interface TicketService extends IGenericService<Ticket>{

}
