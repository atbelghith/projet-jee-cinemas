package com.atb.cinema.service;

import java.io.Serializable;
import java.util.List;

public class PageService<T> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<T> content;
	private int page;
	private int numberOfElements;
	private int totalElements;
	private int totalPages;
	public List<T> getContent() {
		return content;
	}
	public void setContent(List<T> content) {
		this.content = content;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getNumberOfElements() {
		return numberOfElements;
	}
	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}
	public int getTotalElements() {
		return totalElements;
	}
	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	
	
	
	
}
