package com.atb.cinema.service;



import com.atb.cinema.entities.Cinema;
import com.atb.cinema.entities.Film;

public interface CinemaService extends IGenericService<Cinema>{
	public  PageService<Film> getFilms( int page, int size);

	

}
