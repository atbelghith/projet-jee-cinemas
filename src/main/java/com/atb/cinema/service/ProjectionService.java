package com.atb.cinema.service;


import com.atb.cinema.entities.Projection;

public interface ProjectionService extends IGenericService<Projection>{
	
}
