package com.atb.cinema.service;

import java.util.List;

import com.atb.cinema.entities.Film;

public interface FilmService extends IGenericService<Film>{


	List<Film> getfilms(Long id);
 

}
