package com.atb.cinema.service;

import java.util.List;

import com.atb.cinema.entities.Categorie;

public interface CategorieService extends IGenericService<Categorie>  {
	
	public  PageService<Categorie> getCategories( int page, int size);

	public List<Categorie> getcategoriefilms(Long id);


	


	

}
