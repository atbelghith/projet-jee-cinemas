package com.atb.cinema.service;

import java.util.List;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.data.domain.Page;

public interface IGenericService<T> {

	public T saveOrUpdate (T t);
	public void deleteById (Long id);
	public void delete (T t);
	public T findById ( Long id);
	public Page<T> findAllPage (int page, int size);
	public List<T> findAll ();
	public T update( Long id, T t);
	public T save(T t);
	

}
