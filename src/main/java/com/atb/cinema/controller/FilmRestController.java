package com.atb.cinema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atb.cinema.entities.Film;
import com.atb.cinema.service.FilmService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "/films")
public class FilmRestController {
	@Autowired
	FilmService filmService;
	
//	@GetMapping("/filmsp")
//	public List<Film> getfilm(){
//		return filmService.getfilms(1L);
//	}
	
	@GetMapping("/films_page")
    public Page<Film> getAllfilms(@RequestParam(value="page", required = false ) int page,
            @RequestParam(value="size", required = false) int size) {
        return  filmService.findAllPage(page, size);
    }
	
	@GetMapping
    public List<Film> getfilms() {
        return  filmService.findAll();
    }
	

}
