package com.atb.cinema.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
//import javax.validation.Valid;
import com.atb.cinema.entities.Categorie;
import com.atb.cinema.entities.Film;
import com.atb.cinema.service.CategorieService;
import com.atb.cinema.service.FilmService;
import com.atb.cinema.service.PageService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "/categories")
public class CategorieRestController {
	@Autowired
	CategorieService categorieService;
	@Autowired
	FilmService filmService;
	
	@GetMapping("/page")
	public PageService<Categorie> getCategories(@RequestParam(value="page", required = false ) int page,
			                                    @RequestParam(value="size", required = false) int size){
		return categorieService.getCategories( page, size);
	}
	
	@GetMapping("/pageable")
    public Page<Categorie> getAllcategories(@RequestParam(value="page", required = false ) int page,
            @RequestParam(value="size", required = false) int size) {
        return  categorieService.findAllPage(page, size);
    }
	
	@GetMapping
    public List<Categorie> getCategories() {
        return  categorieService.findAll();
    }
	@GetMapping("/categories/{id}")
    public Categorie getCategoriesById(@PathVariable(name="id", required = true ) Long id) {
        return  categorieService.findById(id);
    }
	@GetMapping(path = "{id}")
    public List<Film> getcategoriefilms(@PathVariable(name="id", required = true ) Long id) {
        return  filmService.getfilms(id);
    }
	//@PreAuthorize("hasAnyRole('ADMIN', 'RECRUITER')")
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE
			  , consumes = MediaType.APPLICATION_JSON_VALUE)
	public Categorie update(@RequestBody Categorie categorie, @PathVariable(value = "id") Long id) {
		return categorieService.update(id ,categorie);
	}
	@PostMapping
	public Categorie save( @RequestBody Categorie c) {
		
		return categorieService.save(c);

	}
//	@DeleteMapping(value = "/{id}")
//	public void  delete(@RequestBody Categorie categorie) {
//		
//		categorieService.delete(categorie);
//	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> deleteById(@PathVariable(value = "id") Long id) {
		
		Map<String, Object> response = new HashMap<>();
		response.put("error", false);
		try {
			categorieService.deleteById(id);
		response.put("error", false);
		response.put("message", "SUCCESS");
		}
		catch(Exception e) {
			response.put("error", true);
			response.put("message", "Error occured while deleting this categorie ");
		}
		return ResponseEntity.ok(response);

	}
}
