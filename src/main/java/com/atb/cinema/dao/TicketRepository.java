package com.atb.cinema.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atb.cinema.entities.Ticket;;

public interface TicketRepository extends JpaRepository<Ticket, Long> {

}
