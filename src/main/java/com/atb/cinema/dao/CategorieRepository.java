package com.atb.cinema.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.atb.cinema.entities.Categorie;

@CrossOrigin("*")
public interface CategorieRepository extends JpaRepository<Categorie, Long> {
	
	
	//@RestResource(path ="/byDesignationPage")
	//public Page <Produit> findByDesignationContains(@Param("mc") String des ,Pageable  pageable);
	//Page<Categorie> findBynameContains(@Param("mc") String name, Pageable pageable );
	List<Categorie> findAllById(Iterable<Long> ids);
	
	@Query("select c from Categorie c join c.films f where f.id = :x")
	List<Categorie> getcategoriefilms(@Param("x") Long id);

	Categorie findByName(String name);

}
