package com.atb.cinema.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.atb.cinema.entities.Film;;

public interface FilmRepository extends JpaRepository<Film, Long> {
	// @RestResource(path ="/byCategorie")
	@Query("select f from Film f where f.categorie.id = :x")
	List<Film> getFilms(@Param("x") Long id);
	
	//une autre solutution pour recuperer les film d'une categorie avec la pagination
	//Page<Film> findByCategorie(Categorie c, Pageable pageable);

}
