package com.atb.cinema.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.atb.cinema.entities.Place;

public interface PlaceRepository extends JpaRepository<Place, Long> {

}
