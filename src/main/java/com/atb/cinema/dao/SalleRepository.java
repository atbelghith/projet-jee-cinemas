package com.atb.cinema.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atb.cinema.entities.Salle;

public interface SalleRepository extends JpaRepository<Salle, Long> {

}
