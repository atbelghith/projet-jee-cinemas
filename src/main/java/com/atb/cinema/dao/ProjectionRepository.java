package com.atb.cinema.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atb.cinema.entities.Projection;;

public interface ProjectionRepository extends JpaRepository<Projection, Long> {

}
