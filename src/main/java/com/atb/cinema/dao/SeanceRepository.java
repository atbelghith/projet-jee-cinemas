package com.atb.cinema.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atb.cinema.entities.Seance;

public interface SeanceRepository extends JpaRepository<Seance, Long> {

}
