package com.atb.cinema.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atb.cinema.entities.Ville;

public interface VilleRepository extends JpaRepository<Ville, Long> {

}
