package com.atb.cinema.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atb.cinema.entities.Cinema;

public interface CinemaRepository extends JpaRepository<Cinema, Long>{

}
